import java.util.Scanner;

class ArrayAndYalunka
{
    
    public static void main(String[] args){
        Array();
        Yalunka();
        System.out.println(" ");
        System.out.println("Сonec programu");
    }

    public static void Array(){
        System.out.println("Massiv");
        Scanner in = new Scanner(System.in);
        System.out.println("Coli4estvo strok:");
        int s = in.nextInt();
        System.out.println("Coli4estvo stolbcov:");
        int c = in.nextInt();
        int[][] array = new int[s][c];
        array[0][0] = 3;
        for (int i = 0; i < s; i++) {
            for (int j = 0; j < c; j++) {
                if (j != 0) {
                    array[i][j] = array[i][j-1] + array[0][0];
                }
                if (i != s-1) {
                    if (j == c-1) {
                        if (array[i+1][0] == 0) {
                            array[i+1][0] = array[i][j] + 3;
                        } 
                    } 
                }
            }
        }
        for (int i = 0; i < s; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(" " + array[i][j] + " ");
            }
            System.out.println();
        }
        in.close();
    }
      
    public static void Yalunka(){
        System.out.println("Yalunka");
        System.out.println("Dlina yalunku:");
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        for(int i = 1;i <= length; i++){
            for(int j = i; j <= length; j++){
                System.out.print(" ");
            }
            for(int j = i; j > 1; j--){
                System.out.print("*");
            }
            for(int j = 1; j <= i; j++){
                System.out.print("*");
            }
            System.out.println();
            if(i == length){
                for(int j = 1; j <= length; j++){
                    System.out.print(" ");
                }
                System.out.print("U");
            }
        }
        in.close();
    }
}

